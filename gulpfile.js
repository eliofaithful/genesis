var browserSync = require('browser-sync').create()
var gulp = require('gulp'),
  clean = require('gulp-clean-css'),
  concat = require('gulp-concat'),
  purge = require('gulp-css-purge'),
  rename = require('gulp-rename'),
  sass = require('gulp-sass'),
  uglify = require('gulp-uglify')
var runSequence = require('run-sequence')

var css_folder = './css/'
var js_watch = [
  './js/plugins.js',
  './js/adon/adon*.js',
]
var sass_watch = ['./stylesheets/*.scss', './stylesheets/**/*.scss']
var distro_css_files = [
  // './css/main.css',
  // './css/normalize.css',
  './node_modules/sanitize.css/sanitize.css',
  // './node_modules/sanitize.css/forms.css',
  // './node_modules/sanitize.css/typography.css',
  // './node_modules/sanitize.css/page.css',
  './css/genesis.css'
]

var dist_js_folder = './dist/js/'
var dist_css_folder = './dist/css/'

gulp.task('build', function(cb) {
  runSequence('build_css', 'distribute_css', 'minify-adons', 'minify-main', cb)
})

// compile sass into CSS and minify
gulp.task('build_css', function() {
  return gulp
    .src('./stylesheets/judge.scss')
    .pipe(
      sass({
        includePaths: sass_watch,
      }),
    )
    .pipe(concat('genesis.css'))
    .pipe(purge({ trim: false }))
    .pipe(gulp.dest(css_folder))
    .pipe(rename('genesis.min.css'))
    .pipe(clean())
    .pipe(gulp.dest(css_folder))
})

gulp.task('distribute_css', function() {
  return gulp
    .src(distro_css_files)
    .pipe(concat('genesis.min.css'))
    .pipe(clean())
    .pipe(gulp.dest(dist_css_folder))
    .pipe(browserSync.stream())
})

// distribute adon scripts & plugins
gulp.task('minify-adons', function() {
  gulp
    .src(js_watch)
    .pipe(uglify())
    .pipe(concat('adons.js'))
    .pipe(gulp.dest(dist_js_folder))
    .pipe(browserSync.stream())
})

// distribute main
gulp.task('minify-main', function() {
  return gulp
    .src('./js/main.js')
    .pipe(uglify())
    .pipe(concat('main.js'))
    .pipe(gulp.dest(dist_js_folder))
    .pipe(browserSync.stream())
})

gulp.task('watch', ['build'], function() {
  browserSync.init({
    server: './',
  })
  gulp.watch(sass_watch, ['build'])
  gulp.watch(js_watch, ['build'])
  gulp.watch('./js/main.js', ['build'])

  gulp.watch(sass_watch).on('change', browserSync.reload)
  gulp.watch(js_watch).on('change', browserSync.reload)
  gulp.watch('./js/main.js').on('change', browserSync.reload)
  gulp.watch(['*.html']).on('change', browserSync.reload)
})

gulp.task('default', ['build', 'watch'])
