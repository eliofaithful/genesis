# appname Credits [DEMO]

## Core! Ta very much.

Links to core frameworks you used.

## These were useful

Links to stackoverflow answers, blogs, guides, books, etc which were particularly helpful... not too many... the ones you relied on.

## Artwork

A place to link credits for public domain or creative open license artwork. This is not just to be nice, but also so that you can find it again if you lose the original!
